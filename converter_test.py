
#var 24 timofeev ivan

from converter import convert

valueInMeters = 10

valueInMM = 10000
valueInSM = 1000
valueInDM = 100
valueInKM = 0.01

def test_MM():
    res = convert(valueInMeters, "м", "мм")
    
    if (res == valueInMM):
        with open("results.txt", "a") as myfile:
            myfile.write("TEST PASSED\n[м] -> [мм]: [10] -> [" + str(res) + "]\n")
    else:
        with open("results.txt", "a") as myfile:
            myfile.write("! ! ! TEST FAILED ! ! !\n[м] -> [мм]: [10] -> [" + str(res) + "]\n")

def test_SM():
    res = convert(valueInMeters, "м", "см")
    
    if (res == valueInSM):
        with open("results.txt", "a") as myfile:
            myfile.write("TEST PASSED\n[м] -> [см]: [10] -> [" + str(res) + "]\n")
    else:
        with open("results.txt", "a") as myfile:
            myfile.write("! ! ! TEST FAILED ! ! !\n[м] -> [см]: [10] -> [" + str(res) + "]\n")

def test_DM():
    res = convert(valueInMeters, "м", "дм")
    
    if (res == valueInDM):
        with open("results.txt", "a") as myfile:
            myfile.write("TEST PASSED\n[м] -> [дм]: [10] -> [" + str(res) + "]\n")
    else:
        with open("results.txt", "a") as myfile:
            myfile.write("! ! ! TEST FAILED ! ! !\n[м] -> [дм]: [10] -> [" + str(res) + "]\n")

def test_KM():
    res = convert(valueInMeters, "м", "км")
    
    if (res == valueInKM):
        with open("results.txt", "a") as myfile:
            myfile.write("TEST PASSED\n[м] -> [км]: [10] -> [" + str(res) + "]\n")
    else:
        with open("results.txt", "a") as myfile:
            myfile.write("! ! ! TEST FAILED ! ! !\n[м] -> [км]: [10] -> [" + str(res) + "]\n")

def test_M():
    res = convert(valueInMeters, "м", "м")

    if (res == valueInMeters):
        with open("results.txt", "a") as myfile:
            myfile.write("TEST PASSED\n[м] -> [м]: [10] -> [" + str(res) + "]\n")
    else:
        with open("results.txt", "a") as myfile:
            myfile.write("! ! ! TEST FAILED ! ! !\n[м] -> [мм]: [10] -> [" + str(res) + "]\n")


with open("results.txt", "a") as myfile:
    myfile.write("RESULT OF TESTING:\n")

test_MM()
test_SM()
test_DM()
test_KM()
test_M()