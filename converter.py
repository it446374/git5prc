
#var 24 timofeev ivan

supportedKeys = {
    "мм": 0.001, 
    "см": 0.01, 
    "дм": 0.1,
    "м": 1,
    "км": 1000 }

def convert(value, fromKey, toKey):
    mpFrom = supportedKeys[fromKey]
    mpTo = supportedKeys[toKey]
    mpOfCorrection = mpFrom / mpTo
    return value * mpOfCorrection
